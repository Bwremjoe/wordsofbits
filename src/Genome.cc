#include "Genome.hh"
#include "Random.hh"
#include "Utility.hh"

extern int bits;
extern map<int, char> CodonMap;
extern string target;
extern int uid_;
extern int Time;

Genome::Genome(int id)
{
	uid = id;
	//cout << "Set uid to " << id << endl;
	parent = NULL;
	generation = -1;
	alive = true;
	coding_bits = 0;
	dist = 99999999; 					// A distance of bits is fitness 0. The closer you are to forming the correct word, the better your fitness.
}

Genome::~Genome()
{
	//cout << "Deconstr" << endl;
	//G.clear();			// Zou niet uit moeten maken volgens  https://stackoverflow.com/questions/35514909/how-to-clear-vector-in-c-from-memory
	//ORFs.clear();
}

void Genome::Initialise_Random()
{
	generation = 0;
	Randomize();
	ReadORFs(false);
}

void Genome::CloneGenome(Genome *p, double slip)		// s = slipping chance
{
	if(p == NULL)
		throw "You cannot clone an empty genome. You did something weird...";
	//cout << "Cloning genome of generation " << p->generation << endl;
	//p->PrintRaw();
	alive = true;

	parent = p;
	for(int i =0;i<p->G.size();i++)
	{
		G.push_back(p->G[i]);
		if(genrand_real1() < slip)
		{
			int slipsize = genrand_real1()< 0.5 ? -bits :bits;
			i += slipsize;	// Slipping causes either a duplication or deletion by moving DNApolymerase back or forward in BITSsizes
		}
	}
	generation = p->generation+1;
	ReadORFs(false);
}

void Genome::PointMutate(double u)
{
	// Bit flips
	for(int i =0;i<G.size();i++)
	{
		if(genrand_real1() < u)
		{
			G[i] = G[i] == 1 ? 0 : 1;
			ReadORFs(false);
		}
	}
}

void Genome::DiscoverStart(double u)
{
	vector<bool> start (bits, 1);
	if(genrand_real1() < u)
	{
		auto insertpos = G.begin() + genrand_int(0,G.size()-1);

		G.insert(insertpos, start.begin(), start.end());

		ReadORFs(false);
	}
}

void Genome::StretchDuplicate(double u)
{
	int start;
	int end;
	if(genrand_real1() < u*G.size())
	{
		auto insertpos = G.begin() + genrand_int(0,G.size()-1);
		int point1 = genrand_int(0,G.size()-1);
		int point2 = genrand_int(0,G.size()-1);
		if(point1<point2) start=point1, end=point2;
		else start=point2;end=point1;

		auto first = G.begin() + start;
		auto last = G.begin() + end;
		vector<bool> ins(first,last);
		G.insert(insertpos, ins.begin(), ins.end());
		ReadORFs(false);
	}
}

void Genome::StretchDelete(double u)
{
	int start;
	int end;
	if(genrand_real1() < u*G.size())
	{
		int point1 = genrand_int(0,G.size()-1);
		int point2 = genrand_int(0,G.size()-1);
		if(point1<point2) start=point1, end=point2;
		else start=point2;end=point1;
		auto first = G.begin() + start;
		auto last = G.begin() + end;
		G.erase(first, first+end);
		ReadORFs(false);
	}
}

void Genome::Duplicate(double u)
{
	int start;
	int end;
	if(genrand_real1() < u*G.size())
	{
		auto insertpos = G.begin() + genrand_int(0,G.size()-1);
		G.insert(insertpos, (genrand_real1() < 0.5)?'1':'0');
		ReadORFs(false);
	}
}

void Genome::Delete(double u)
{
	int start;
	int end;
	if(genrand_real1() < u*G.size())
	{
		auto delpos = G.begin() + genrand_int(0,G.size()-1);
		G.erase(delpos);
		ReadORFs(false);
	}
}


void Genome::Randomize()
{
	G.resize(140);
	for (int i = 0;i < G.size(); ++i)					// Random bit sequence
        G[i] = genrand_int(0,1);
	for (int i= 10;i < min((int)G.size(),10+bits); ++i)		// Insert "start-codon" of ten 1's
     	G[i] = 1;
	for (int i= 100;i < min((int)G.size(),100+bits); ++i)		// Insert "stop-codon" of ten 1's
			G[i] = 0;
	// for (int i= 1010;i < min((int)G.size(),1020); ++i)		// Insert "start-codon" of ten 1's
  //       G[i] = 1;
	// for (int i= 1100;i < min((int)G.size(),1120); ++i)		// Insert "stop-codon" of ten 1's
	// 			G[i] = 0;
}

void Genome::ReadORFs(bool V)
{
	if(V) cout << "//////////////////////////////" << endl;
	if(V) cout << "/////////READING ORFS/////////" << endl;
	if(V) cout << "//////////////////////////////" << endl;
	string protein = "";
	int numones = 0;
	bool reading = 0;
	int scanpos = 0;
	string stop (bits, '0');

	// Scan the genome for consecutive 1's with length <bits>, and if found
	// make proteins of the following chunks of size <bits>
	// if a stopcodon is found, add the protein to the list, and
	// start scanning for 1's again

	for(int i =0; i < G.size(); i++)
	{
		if(V)	cout << StringRepr() << endl;
		if(V)	for(int p=0;p<i;p++) cout << " ";
		if(V)	cout << "^";
		if(V)	cout << endl;

		if(numones==bits)
		{
			if(V) cout << "Found a start codon" << endl;
			if(V)	cout << StringRepr() << endl;
			if(V)	for(int p=0;p<i;p++) cout << " ";
			if(V)	cout << "^";
			reading = 1;
			scanpos = i;   //i-bits; if you also want ajacent ORFs but it gives a bit too many ORFs
			if(V) cout << "Set scanpos for next iteration to " << scanpos << endl;
			numones=0;
		}


		if(reading)
		{
			if(V)	cout << endl;
			if(V)	cout << "Reading ORF. Now at pos " << i << endl;

			string key = "";
			for(int c=0;c<bits;c++)
			{
				coding_bits++;
				key = key + (G[i+c]==0?'0':'1');
				if (i+c >= G.size())
				break;
			}
			if(V) cout << "Got key: " << key << endl;
			if(key.compare(stop) == 0 || key.size() < bits)	// Stop at stop codon OR when no more BITlength piece is left
			{
				if(V)	cout << "STop!" << endl;
				reading=0;
				//i = scanpos;
				if(V) cout << "Continueing scan from " << i << endl;
				if(V)	cout << "Done. This is the protein: " << protein << endl;
				if(V)	cout << "Distance: " << lvdist(protein,target) << endl;
				ORFs.push_back(protein);
				protein.clear();
			}
			else{
				protein = protein + CodonMap[getKey(key)];
				if(V)	cout << "found letter: " << CodonMap[getKey(key)] << endl;
			}
			if(reading)i+=bits-1;

		}
		else			// Scanning for 1's
		{
			if(G[i] == 1) numones++;
			else numones=0;
		}



	}

	protein.clear();
	stop.clear();

	int mindist = 9999999;
	for(int i = 0;i<ORFs.size();i++)
	{
		int thisdist = lvdist(target,ORFs[i]);
		if(V) cout << "ORF:\t" << ORFs[i] << endl;
		if(V) cout << "TAR:\t" << target << endl;
		if(V) cout << "Dist:\t" << lvdist(target,ORFs[i]);
		if(thisdist < mindist)
			mindist = thisdist;

		if(V) cout << endl;
		if(V) cout << endl;

	}
	if(V) cout << "Done with this individual" << endl;
	if(V) cout << mindist << endl;
	dist = mindist;

}

string Genome::StringRepr()
{
	string r;
	if (this == NULL)
	{
		return "(no genome here!)";
	}
	for (int i = 0;i < G.size(); ++i)
      r = r + ((G[i]==0)?'0':'1');
	return r;
}
