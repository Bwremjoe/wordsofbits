#include "Header.hh"			// Library includes, namespaces etc.
#include "Random.hh"			// Random number generation (mersenne.h)
#include "World.hh" 			// Global variables
#include "Utility.hh"           // Functions for doing clever shizzle

string env1 = "THERE IS GRANDEUR IN THIS VIEW OF LIFE WITH ITS SEVERAL POWERS, HAVING BEEN ORIGINALLY BREATHED INTO A FEW FORMS OR INTO ONE; AND THAT, WHILST THIS PLANET HAS GONE CYCLING ON ACCORDING TO THE FIXED LAW OF GRAVITY, FROM SO SIMPLE A BEGINNING ENDLESS FORMS MOST BEAUTIFUL AND MOST WONDERFUL HAVE BEEN AND ARE BEING EVOLVED.";
string env2 = "RA RA RASPUTIN LOVER";
string target = "";

int Time;

////// TODO
/// MULTIPLE GENRANDS!!
// Multiple start sites (hij begint nu aan het einde van orf met zoeken)
// The main loop


int main(int argc, char **argv)
{
    target = env1;
    Setup();                  // Everything required for setup (init genrand, setup the grid, fill it with genomes, etc.)
    Setup_Grid();
    cout << "\n\n~ Now entering main loop ~\n\n" << endl;

    Time = 1;
    while(Time <= MaxTime)
    {

        // if(genrand_real1() < 0.0)
		// {
        //     (target.compare(env1) == 0) ? target = env2 : target = env1;
		// 	loopgrid(i,j) if(grid[i][j] != NULL) grid[i][j]->ReadORFs();
		// }
        Simulation_Step(Time%100==0);    // Death, reproduction, etc. (synchronous)



        Time++;
    }


    cout << "\n\n All is done... for now... \n\n" << endl;
    return(0);
}


// Above is the main loop, below are all the "world" functions
void Simulation_Step(bool V)
{

    loopgrid(i,j) grid_old[i][j] = grid[i][j];	// Old grid to have a back-up where the actual empty spots are

    Genome * best = NULL;
    int best_dist = -1;
    int sum_living = 0;
    int sum_gsize = 0;
    int sum_gens = 0;
    //cout << set_Col(3);
    loopgrid(i,j)
    {
        //cout << "|";
        if(grid[i][j] != NULL)
        {
            if(grid[i][j]->dist < best_dist || best_dist < 0) // If improved or the first individual, store best dist
            {

                best_dist = grid[i][j]->dist;
                best = grid[i][j];
            }
            sum_gsize += grid[i][j]->G.size();
            sum_gens += grid[i][j]->generation;
            sum_living++;
        }
        //else
            //cout << 'x';

        //cout << "|";
        //if(j==ncol-1)
        //cout << endl;

    }

    //if(V) cout << set_Col(0);
    if(V) cout << "Time: " << Time << " (" << (double)sum_gens/sum_living << ")" << endl;
    if(V) cout << "Target: " << target << endl;
    if(V) cout << "Popsize: " << sum_living << endl;
    if(V) cout << "Best dist: " << best_dist << endl;
    if(V) string best_codon = "";
    if(V)
    for(int q = 0; q<best->ORFs.size();q++)
        cout << q << ":\t" << best->ORFs[q] << "\n\n";
    map<string,int> samephenotype;
    map<string,int> all_genotypes;
    if(V)
    loopgrid(i,j)
      if(grid[i][j]!=NULL)
        for(int q = 0; q<grid[i][j]->ORFs.size();q++)
        {
            all_genotypes[grid[i][j]->StringRepr()]++;
            if(lvdist(grid[i][j]->ORFs[q],target) == best_dist)
              samephenotype[grid[i][j]->StringRepr()]++;
        }
    if(V) cout << endl;
    // Dit klopt nog niet helemaal. Moet als fractie van alle genotypes zijn ofzo. tbc
    if(V) cout << "Frac same fitness genotypes: " << (double)samephenotype.size()/all_genotypes.size() << endl;
    //cout << "" << endl;
    if(V) cout << "Avrg genome size: " << (double)sum_gsize/sum_living << "\t";

    loopgrid(i,j)
    {
        if(grid[i][j]!=NULL)
        {
            if(genrand_real1() < 0.1)
            {
                grid[i][j]->alive = false;	// Marked for death, but can still reproduce
            }
        }
        else
        {
            CompeteMoore(i,j,false);
        }
    }
    if(V) cout << endl << endl;
    loopgrid(i,j)
    {
        if(grid[i][j]!=NULL)
        {
            if(grid[i][j]->alive != true)
            {
                delete grid[i][j];
                grid[i][j] = NULL;
            }
        }
    }
}
void Setup()
{
    init_genrand(57);
    Setup_CodonMap();
    Setup_Grid();
}

void Setup_Grid()
{
    loopgrid(i,j)
    {
        if(genrand_real1() < 0.1)
        {
            grid[i][j] = new Genome(uid_++);
            grid[i][j]->Initialise_Random();
        }
        grid_old[i][j] = NULL;
    }
}

void Setup_CodonMap()
{
    int perm = pow(2,bits);
    int all_ints[perm];                 // Using 32 characters A-Z, !, ?,
    int n = 32;          // use first n letters of the list for mapping

    for(int i = 0; i<perm; i++)
    all_ints[i] = i;    // full ones and full zero's are start and stop-sites, so are not in there

    int count_vals[32] = {0};
    char letters[32] = {'A','B','C','D','E','F','G','H','I',
    'J','K','L','M','N','O','P','Q','R',
    'S','T','U','V','W','X','Y','Z',' ',
    '.',',',';','!','?'};

    shuffleArray(all_ints, perm);


    for(int i = 0; i<perm; i++)
    {
        int key = all_ints[i];
        int letter = n*((float)i/perm);
        count_vals[letter]++;
        char val = letters[letter];
        CodonMap[key] = val;
    }

    for(int i = 0; i < n; i++)
    cout << letters[i] << " used " << count_vals[i] << " times." << endl;
    cout << endl;

    int pathlength = 0;
    map<char,int> neutral_count;
    for(int i = 0; i < perm; i++)
    {
        string curr = toBinary(i);
        string next = "";
        cout << CodonMap[getKey(curr)] << ":" << curr << "\t" << getKey(curr) << "\t";
        int numeq = 0;
        for(int q = 0; q < bits; q++)
        {
            string mut = curr;
            (mut[q] == '0') ? mut[q] = '1' : mut[q] = '0';
            //cout << mut << endl;
            cout << CodonMap[getKey(mut)] << ", ";
            if(CodonMap[getKey(mut)] == CodonMap[getKey(curr)])
            {
                neutral_count[CodonMap[getKey(curr)]]++;
                numeq++;
            }
        }
        cout << "(" << numeq << " neutral)";
        cout << endl;
    }

    for(int q = 0; q < n;q++) cout << letters[q] << ":" << neutral_count[CodonMap[q]]/2 << "\t";
    cout << endl;
    //int q = getKey("0000000010");
    //cout << q << CodonMap[q] << endl;
}

void CompeteMoore(int i, int j, bool V)
{
    // 1) First select winner based on distance to Target word
    // 2) Reproduce and mutate into empty spot
    if(V) cout << "Competing for empty in point " << i << "," << j << endl;

    //double cumfit[9];         // Store fitnesses of (potentially 8 competitors + empty claim)
    vector<Genome*> competitors;  // Store competitors in temp vector


    int p = 1;

    loopmoore(k,l)            // Loop from (-1,-1) to (1,1) excluding (0,0)
    {
        int ci = (i-k+nrow)%nrow;    // Coordinates i
        int cj = (j-l+ncol)%ncol;    // Coordinates j

        if(V)cout << "Competitor from " << ci << "," << cj << endl;

        if(grid_old[ci][cj] != NULL)
        {
            if(V)cout << "Distance of this one: " << grid_old[ci][cj]->dist << endl;
            competitors.push_back(grid_old[ci][cj]);
        }
        p++;
    }
    if(competitors.size()==0) return;
    double rand_winner = genrand_real1();
    if(V)cout << endl << endl;
    if(V)cout << "Number of competitors: " << competitors.size() << endl;

    // First shuffle, then sort. Guarantees individuals with similar fitnesses to be random w.r.t. one another
    shuffleVector(competitors);
    sort(competitors.begin(),competitors.end(), CompDist);

    double scale = 1.0-pow(genrand_real2(),2.0);      // genrand_real2 excludes 1.0 since it is a range
    if(V)cout << "Scale; " << scale << endl;
    int win = competitors.size()*scale;   // Pow 20 = the fittest almost always wins, even with small advantages
    if(V)cout << "Selected num as winner: " << win << endl;
    Genome * winner = competitors[win];
    if(V)cout << "Winner dist: " << winner->dist << endl;

    if(winner != NULL)
    {
        if(V)cout << "Reproducting " << winner << endl;
        grid[i][j] = new Genome(uid_++);
        grid[i][j]->CloneGenome(winner, 0.000001);
        grid[i][j]->PointMutate(0.00001);
		grid[i][j]->Delete(0.000001);       // Per bit
        grid[i][j]->Duplicate(0.000001);    // Per bit

        // One of the two below sometimes segfaults when there are small genomes... tbc
		grid[i][j]->StretchDelete(0.0000001);       // Per bit, but only once
        grid[i][j]->StretchDuplicate(0.0000001);    // Per bit but only once


        grid[i][j]->DiscoverStart(0.00); // per generation
        if(V)cout << "Cloned " << winner << endl;
    }
    else
    {
        if(V) cout << "Competitor NULL!";
    }
    if(V)cout << endl << endl;
}
