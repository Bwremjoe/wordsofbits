#include "Utility.hh"
#include "Random.hh"

extern int bits;

// Colours for output
string set_Col(int col)
{
    stringstream s;
    s << "\033[";
    if (col) {
        s << 29 + col;
    }
    s << "m";
    return s.str();
}

// A utility function to swap two integers
void swap (int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void shuffleArray(int arr[], int n)			// fisherYatesShuffling https://www.sanfoundry.com/cpp-program-implement-fisher-yates-algorithm-array-shuffling/
{
    // Start from the last element and swap one by one. We don't
    // need to run for the first element that's why i > 0
    for (int i = n-1; i > 0; i--)
    {
        // Pick a random index from 0 to i
        int j = genrand_int(0,512927377) % (i+1);           // 512,927,377 is a nice large prime
        //int j = rand() % (i+1);
        // Swap arr[i] with the element at random index
        swap(&arr[i], &arr[j]);
    }
}

void shuffleVector(vector<Genome*> &v)			// fisherYatesShuffling https://www.sanfoundry.com/cpp-program-implement-fisher-yates-algorithm-array-shuffling/
{
    int n = v.size();

    for (int i = n-1; i > 0; i--)
    {
        int j = genrand_int(0,512927377) % (i+1);           // 512,927,377 is a nice large prime
        swap(v[i], v[j]);
    }
}

int getKey(string str)
{
    if(str.size() != bits)
        cout << "Error. This has to be " << bits << "\n";
    else
        return stoi(str,nullptr,2);
}

string toBinary(int n)
{
    string r;
    while(n!=0) {r=(n%2==0 ?"0":"1")+r; n/=2;}
    int pad = bits-r.size();
    while(pad>0)
        r = '0' + r, pad--;
    return r;
}

//https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C++
// lvdist = de minimale hoeveelheid bewerkingen die nodig is om de ene string in de andere te veranderen
// waarbij substituties, deleties, en inserties worden gebruikt
unsigned int lvdist(const std::string &s1, const std::string &s2)
{
    const size_t substitution = 1;
    const size_t deletion = 3;
    const size_t insertion = 3;

    const size_t len1 = s1.size(), len2 = s2.size();
    vector<vector<unsigned int> > d(len1 + 1, vector<unsigned int>(len2 + 1));
    d[0][0] = 0;
    for(unsigned int i = 1; i <= len1; ++i) d[i][0] = deletion * i;
    for(unsigned int i = 1; i <= len2; ++i) d[0][i] = insertion * i;

    for(unsigned int i = 1; i <= len1; ++i)
        for(unsigned int j = 1; j <= len2; ++j)
            d[i][j] = std::min(
                std::min(d[i - 1][j] + deletion, d[i][j - 1] + insertion),
                d[i - 1][j - 1] + (s1[i - 1] == s2[j - 1] ? 0 : substitution)
            );
    return d[len1][len2];
}

bool CompDist(Genome * a, Genome * b)
{
	return a->dist > b->dist;
}
