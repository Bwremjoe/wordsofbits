#ifndef Utilityheader
#define Utilityheader

#include "Header.hh"
#include "Genome.hh"

void swap(int *a, int *b);					// A function to swap two integers
void shuffleArray(int *arr, int n);			// fisherYatesShuffling: https://www.geeksforgeeks.org/shuffle-a-given-array/
void shuffleVector(vector<Genome*> &v);		// fisherYatesShuffling: https://www.geeksforgeeks.org/shuffle-a-given-array/

int getKey(string str);						// Converts 10-bit string into integer for keying the map of letters

string toBinary(int n);
string set_Col(int col);
unsigned int lvdist(const std::string& s1, const std::string& s2);

bool CompDist(Genome * a, Genome * b);

#endif
