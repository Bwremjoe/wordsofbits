#ifndef GenomeHeader
#define GenomeHeader

#include "Header.hh"

class Genome
{
    public:
        int uid;			// Unique ID
        int coding_bits;            // No coding bits
        vector < string > ORFs;     // All "proteins" in a list

        int generation;               // nth generation
        bool alive;
        double dist;

        Genome * parent;		// Pointer to parent genome (NULL if first)
        vector< bool > G;
        Genome(int id);		         // Genome constructor
        ~Genome();			        // Genome destructor
        void Initialise_Random();
        void CloneGenome(Genome *Parent, double slip);   // Custom copy constructor (deep copy based on parent)
        string StringRepr();
        void Randomize();
        void ReadORFs(bool V=false);      // Returns string with ORFs turned to letters

        void PointMutate(double u);       // Per bit
        void DiscoverStart(double u);
		void Duplicate(double u);         // Per bit
		void Delete(double u);         // Per bit
        void StretchDuplicate(double u);         // Per bit, but only once per generation
        void StretchDelete(double u);         // Per bit, but only once per generation


};

#endif
