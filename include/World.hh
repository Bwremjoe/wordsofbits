#include "Genome.hh"
#include "Utility.hh"

/////////////////////////////
////	Global variables ////
/////////////////////////////
long int uid_=1;									// Counter for UIDs

int MaxTime = 10000000;									// Maxtime for running

const int nrow = 10;
const int ncol = 10;
int bits = 10;								// Number of bits for codons.

Genome* grid[nrow][ncol];							// The grid containing all genomes
Genome* grid_old[nrow][ncol];						// Used for syncronous updating

map<int, char> CodonMap;						// Mapping of all possible 10-bit sequences to alphabetic letters

#define loopgrid(i,j) for(int i = 0; i < nrow; ++i) for(int j = 0; j < ncol; ++j)
#define loopmoore(k,l) for(int k = -1; k <= 1; ++k) for(int l = -1; l <= 1; ((l + 1) == 0 && k ==0) ? (l+=2) : (l++))		// Loop over moore neighbourhood without self

long long genrand_real1_offset = 0LL;				// Counter for genrand_real1 (for reloading)
long long genrand_real2_offset = 0LL;				// Counter for genrand_real2 (for reloading)

void Setup();
void Setup_Grid();
void Setup_CodonMap();
void Simulation_Step(bool V);
void CompeteMoore(int, int, bool V=false);
